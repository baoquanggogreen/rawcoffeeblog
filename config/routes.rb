Rails.application.routes.draw do
  resources :coffees
  root 'home#index'

  resources :sessions, only: [:new, :create, :destroy]
  resources :users
  get 'signup', to: 'users#new', as: 'signup'
  get 'login', to: 'sessions#new', as: 'login'
  get 'logout', to: 'sessions#destroy', as: 'logout'
  get 'homepage', to: 'home#homepage', as: 'homepage'
end
