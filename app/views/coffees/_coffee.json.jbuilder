json.extract! coffee, :id, :name, :price, :origin, :description, :created_at, :updated_at
json.url coffee_url(coffee, format: :json)
