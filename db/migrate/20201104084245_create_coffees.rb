class CreateCoffees < ActiveRecord::Migration[6.0]
  def change
    create_table :coffees do |t|
      t.string :name
      t.string :price
      t.string :origin
      t.text :description

      t.timestamps
    end
  end
end
